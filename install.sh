#!/bin/bash -x

set -e -o pipefail

rel="https://api.github.com/repos/actions/runner/releases"
tag=$(curl -s $rel | jq -r '.[] | select(.prerelease != true) | .tag_name' | sort -n | tail -n 1)
tar="actions-runner-linux-x64-${tag:1}.tar.gz"
url="https://github.com/actions/runner/releases/download/${tag}/${tar}"

echo "Installing: $tag"

mkdir /runner
curl -L "${url}" | tar xzC /runner

