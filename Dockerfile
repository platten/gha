FROM debian:latest
COPY install.sh /root/
RUN apt update && apt install -y curl jq
RUN /root/install.sh

FROM registry.gitlab.com/enarx/dev:latest
COPY --from=0 /runner /runner
COPY start.sh /root/
CMD /root/start.sh
